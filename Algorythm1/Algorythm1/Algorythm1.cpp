﻿// Algorythm1.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <limits>

//Структура в которой хранятся позиции слов в тексте
struct Positions
{
	std::vector<int> firstWordPositions;
	std::vector<int> secondWordPositions;
};

Positions GetWordsPositionsFromFile(std::string const& fileName, std::string const& word1, std::string const& word2)
{
	Positions positions;
	std::fstream inputFile(fileName);
	std::string line;
	//номер слова
	size_t wordsCount = 0;
	while (std::getline(inputFile, line))
	{
		std::stringstream sstream(line);
		std::string word;
		//считываем слова и записываем их позиции в соответствующий массив
		while (sstream >> word)
		{
			if (word == word1)
			{
				positions.firstWordPositions.push_back(wordsCount);
			}
			else if (word == word2)
			{
				positions.secondWordPositions.push_back(wordsCount);
			}
			++wordsCount;
		}
	}
	return positions;
}

struct MinMaxDistance
{
	int min = INT32_MAX;
	int max = 0;

	void SetMin(int value)
	{
		if (min > value)
		{
			min = value;
		}
	}

	void SetMax(int value)
	{
		if (max < value)
		{
			max = value;
		}
	}
};

MinMaxDistance CalculateMinMaxDistanceBetween2Words(std::string const& fileName, std::string const& word1, std::string const& word2)
{
	//получаем позиции заданых слов
	auto positions = GetWordsPositionsFromFile(fileName, word1, word2);
	MinMaxDistance minmaxDistance;
	//находим минимальное и максимальное расстояние путем сравнения 
	for (auto i : positions.firstWordPositions)
	{
		for (auto k : positions.secondWordPositions)
		{
			minmaxDistance.SetMin(std::abs(i - k) - 1);
			minmaxDistance.SetMax(std::abs(i - k) - 1);
		}
	}
	return minmaxDistance;
}

int main()
{
	auto distance = CalculateMinMaxDistanceBetween2Words("input.txt", "hello", "world");
	std::cout << "Min distance is " << distance.min << std::endl << "Max distance is " << distance.max << std::endl;
	return 0;
}