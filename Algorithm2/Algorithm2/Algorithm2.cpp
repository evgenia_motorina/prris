// Algorithm2.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
#include <vector>
#include <sstream>
#include <map>
#include <algorithm>

using NoteBook = std::map<char, std::vector<std::string>>;

//разбивает строку на слова
std::vector<std::string> GetCommandByWords(std::string const& command)
{
	std::vector<std::string> words;
	std::stringstream sstream(command);
	std::string str;
	while (sstream >> str)
	{
		words.push_back(str);
	}
	return words;
}

//добавляет запись в массив записей
void AddNote(NoteBook& notes, std::string const& note)
{
	//поиск блока по первой букве
	auto it = notes.find(note[0]);
	if (it == notes.end())
	{
		//если блок не найден, он создается с 1 записью 
		notes.insert({ note[0], std::vector<std::string>({ note }) });
		return;
	}
	//есди блок найден - в него дозаписывается запись
	it->second.push_back(note);
}

//подсчет количества записей с заданным префиксом
int GetNotesWithPrefCount(NoteBook& notes, std::string const& pref)
{
	auto it = notes.find(pref[0]);
	//если блока по первой букве не существует, то записей 0
	if (it == notes.end()) return 0;
	//иначе проводится подсчет записей с требуемым префиксом
	return	std::count_if(it->second.begin(), it->second.end(), [pref](std::string const& note) {return note.size() >= pref.size() && note.substr(0, pref.size()) == pref; });
}

int main()
{
	NoteBook notes;
	std::string str;
	while (!std::cin.eof() && !std::cin.fail())
	{
		std::getline(std::cin, str);
		//разбиваем введенную строку на слова
		auto words = GetCommandByWords(str);
		// слов в комманде должно быть 2
		if (words.size() != 2) continue;

		if (words[0] == "Add")
		{
			//добавление записи
			AddNote(notes, words[1]);
		}
		else if (words[0] == "Find")
		{
			//подсчет записей с заданым префиксом и вывод информации
			std::cout << words[1] << " -> " << GetNotesWithPrefCount(notes, words[1]) << std::endl;
		}

	}
	return 0;
}
