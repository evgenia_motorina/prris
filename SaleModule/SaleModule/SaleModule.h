#pragma once
#include "Cart.h"
#include <vector>
#include <set>

//������ ������
class SaleModule
{
public:
	//���������� ���������
	double CalculatePriceWithSale(Cart& cart);

private:
	//������ �� ������ �������
	double MakeSaleForGroup(std::vector<ProductType> products, std::map<ProductType, int>& cart, int saleInPercent, bool oneTime = false);
	//������ �� 1 �� ������� ��� ������� 1 ������������� �������� � ������ ������������ 
	double MakeSaleForOneOf(ProductType requiredProduct, std::vector<ProductType> possibleProducts, std::map<ProductType, int>& cart, int saleInPercent);
	//������ �� ��� ��������
	double MakeSaleForAll(std::map<ProductType, int>& cart);

	//�������� ����� �� ����������� ������ 
	bool CheckIfSalePossible(std::vector<ProductType> productsNeeded, std::map<ProductType, int> const& cart);
	//������� �� ������� �������� �� ������� ����������� ������
	void DecrementProductsCount(std::vector<ProductType> productsNeeded, std::map<ProductType, int> & cart);
	//�������� ���� �� �������
	double GetSaledPrice(std::vector<ProductType> products, int saleInPercent);
	double GetSaledPrice(double price, int saleInPercent);

	//�������� ������ ��������� ��������� ��� ��������� ����� ���������
	double GetFullPriceWithoutSome(std::map<ProductType, int> & cart, std::set<ProductType> notAccountedproducts);
	//�������� ������ ��������� ��������� ���������
	double GetFullPriceOnlySome(std::map<ProductType, int> & cart, std::set<ProductType> accountedproducts);

};

