#include "pch.h"
#include "ProductStorage.h"

namespace
{
	//����� ���� �� ��� ���� ���������
	const std::map<ProductType, double> priceList = {
		{ProductType::A, 19.0}, {ProductType::B, 17.0}, {ProductType::C, 6.0}, {ProductType::D, 8.0},
		{ProductType::E, 12.0}, {ProductType::F, 3.0}, {ProductType::G, 7.0}, {ProductType::H, 14.0},
		{ProductType::I, 20.0}, {ProductType::J, 11.0}, {ProductType::K, 15.0}, {ProductType::L, 9.0},
		{ProductType::M, 6.0}
	};
}

Product ProductStorage::GetproductByType(ProductType type)
{
	auto productInfo = priceList.find(type);
	//���� ������� � ����� �����
	if (productInfo != priceList.end())
	{
		//���������� ���������� � �������� 
		return Product(type, productInfo->second);
	}
	//���� ������� �� ��������� �� ���������� ����������� ������� � ����� -1
	return Product(ProductType::UnknownType, -1.0);
}
