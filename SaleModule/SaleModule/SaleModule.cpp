#include "pch.h"
#include "SaleModule.h"

double SaleModule::CalculatePriceWithSale(Cart & cart)
{
	//��������� �� �������
	double saledPrice = 0.0;
	//�������� ���������� �������
	auto products = cart.GetAllProducts();
	//��������� ��������� � ������� �������, ���� �������� (����� ������ 0, ���� ������ �� ��������)
	saledPrice += MakeSaleForGroup({ ProductType::A, ProductType::B }, products, 10);
	//2 �������
	saledPrice += MakeSaleForGroup({ ProductType::D, ProductType::E }, products, 5);
	// 3 �������
	saledPrice += MakeSaleForGroup({ ProductType::E, ProductType::F, ProductType::G }, products, 5);
	//4 �������
	saledPrice +=  MakeSaleForOneOf(ProductType::A, { ProductType::K, ProductType::L, ProductType::M }, products, 5);
	//5,6,7 �������
	saledPrice +=  MakeSaleForAll(products);
	return saledPrice;
}

double SaleModule::MakeSaleForGroup(std::vector<ProductType> products, std::map<ProductType, int> & cart, int saleInPercent, bool oneTime)
{
	double price = 0.0;
	//���� ����� ��� ����������� ��������������� ���������� ��� ��� ���� ��������� �������
	bool firstTime = true;
	//��������� ������, ���� ��� �������� ������� ��� ������� ���������
	while ((!oneTime || firstTime) && CheckIfSalePossible(products, cart))
	{
		DecrementProductsCount(products, cart);
		price += GetSaledPrice(products, saleInPercent);
		firstTime = false;
	}
	//���� �� �������
	return price;
}

double SaleModule::MakeSaleForOneOf(ProductType requiredProduct, std::vector<ProductType> possibleProducts, std::map<ProductType, int>& cart, int saleInPercent)
{
	for (auto it : possibleProducts)
	{
		std::vector<ProductType> products = { requiredProduct , it };
		if (CheckIfSalePossible(products, cart))
		{
			DecrementProductsCount(products, cart);
			//������ ����������� ������ �� 1 �����
			return GetSaledPrice({it}, saleInPercent) + ProductStorage::GetproductByType(requiredProduct).GetPrice();
		}
	}
	return 0.0;
}

double SaleModule::MakeSaleForAll(std::map<ProductType, int>& cart)
{
	std::set<ProductType> notAccountedproducts = { ProductType::A, ProductType::C };
	int productsCount = 0;
	for (auto& it : cart)
	{
		productsCount += it.second;
	}
	double saledPrice = 0.0;
	//������ ������� �� ���������� �������
	//3-5%
	//4-10%
	//5-20%
	switch (productsCount)
	{
	case 3: saledPrice = GetSaledPrice(GetFullPriceWithoutSome(cart, notAccountedproducts), 5); break;
	case 4: saledPrice = GetSaledPrice(GetFullPriceWithoutSome(cart, notAccountedproducts), 10); break;
	case 5: saledPrice = GetSaledPrice(GetFullPriceWithoutSome(cart, notAccountedproducts), 20); break;
	default: saledPrice = GetFullPriceWithoutSome(cart, notAccountedproducts); break;
	}
	//�������� ��������� ���� �� �������� �� ������� ���������������� ������ + ������ ���� �� ����������� ������
	return saledPrice + GetFullPriceOnlySome(cart, notAccountedproducts);
}

bool SaleModule::CheckIfSalePossible(std::vector<ProductType> productsNeeded, std::map<ProductType, int> const& cart)
{
	for (auto product : productsNeeded)
	{
		auto it = cart.find(product);
		if (it == cart.end() || it->second < 1)
		{
			return false;
		}
	}
	return true;
}

void SaleModule::DecrementProductsCount(std::vector<ProductType> productsNeeded, std::map<ProductType, int>& cart)
{
	for (auto product : productsNeeded)
	{
		auto it = cart.find(product);
		if (it != cart.end() || it->second > 0)
		{
			--(it->second);
		}
	}
}

double SaleModule::GetSaledPrice(std::vector<ProductType> products, int saleInPercent)
{
	double priceWithoutSales = 0;
	for (auto product : products)
	{
		priceWithoutSales += ProductStorage::GetproductByType(product).GetPrice();
	}
	return GetSaledPrice(priceWithoutSales, saleInPercent);
}

double SaleModule::GetSaledPrice(double price, int saleInPercent)
{
	return price * (double(100 - saleInPercent) / 100.0);
}

double SaleModule::GetFullPriceWithoutSome(std::map<ProductType, int>& cart, std::set<ProductType> notAccountedproducts)
{
	double price = 0.0;
	for (auto& it : cart)
	{
		if (notAccountedproducts.find(it.first) == notAccountedproducts.end())
		{
			price += ProductStorage::GetproductByType(it.first).GetPrice() * it.second;
		}
	}
	return price;
}

double SaleModule::GetFullPriceOnlySome(std::map<ProductType, int>& cart, std::set<ProductType> accountedproducts)
{
	double price = 0.0;
	for (auto& product : accountedproducts)
	{
		auto it = cart.find(product);
		if (it != cart.end() && it->second > 0)
		{
			price += ProductStorage::GetproductByType(it->first).GetPrice() * it->second;
		}
	}
	return price;
}
