#pragma once
#include "ProductStorage.h"

//������� � ���������� 
class Cart
{
public:
	Cart();
	~Cart();

	//���������� �������� � �������
	void Add(ProductType type, int count);

	//��������� ������ ��������� � �������
	std::map<ProductType, int> const & GetAllProducts() const;

	//��������� ���������� ��������� � �������
	int GetProductsCount() const;

private:
	std::map<ProductType, int> m_productsInCart;
	int m_count = 0;
};

