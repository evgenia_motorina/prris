#include "pch.h"
#include "Cart.h"


Cart::Cart()
{
}


Cart::~Cart()
{
}

void Cart::Add(ProductType type, int count)
{
	//����� ��� �� ����� ��� �������� � �������
	auto it = m_productsInCart.find(type);
	//������� ���������� ����������� ���������
	int countInCart = count;
	if (it != m_productsInCart.end())
	{
		//���� ��� ����� ������� � �������, ��� ���-�� ������������
		countInCart += it->second;
	}
	//�������� ������ ���������� ��������� �������� � �������
	m_productsInCart.insert_or_assign(type, countInCart);
	//������� ������ ����� ���������
	m_count += count;
}

std::map<ProductType, int> const & Cart::GetAllProducts() const
{
	return m_productsInCart;
}

int Cart::GetProductsCount() const
{
	return m_count;
}
