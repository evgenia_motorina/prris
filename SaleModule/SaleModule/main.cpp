#include "pch.h"
#include <iostream>
#include "SaleModule.h"

int main()
{
	//заполнение корзины продуктами
	Cart cart;
	cart.Add(ProductType::A, 2);
	cart.Add(ProductType::B, 1);
	cart.Add(ProductType::C, 2);
	cart.Add(ProductType::D, 0);
	cart.Add(ProductType::H, 1);
	cart.Add(ProductType::M, 3);

	SaleModule saleModule;
	//вычисление и вывод цены со скидками
	std::cout << "Price is " << saleModule.CalculatePriceWithSale(cart) << std::endl;

	return 0;
}
