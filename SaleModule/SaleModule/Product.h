#pragma once

//��� ��������
enum class ProductType {
	UnknownType = 0,
	A, B, C, D, E, 
	F, G, H, I, 
	J, K, L, M
};

//�������
class Product
{
public:
	Product(ProductType type, double price)
		: m_type(type)
		, m_price(price)
	{
	}

	//��������� ����
	ProductType GetType() const
	{
		return m_type;
	}

	//��������� ��������� ��������
	double GetPrice() const
	{
		return m_price;
	}

private:
	ProductType m_type;
	double m_price = 0;
};

